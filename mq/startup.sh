#!/bin/sh

set -e

rabbitmq-plugins enable --offline rabbitmq_management
su -s /bin/sh -c '/usr/sbin/rabbitmq-server' rabbitmq &

sleep 5

rabbitmqctl list_users --quiet | grep -q gnuviechadmin || \
  rabbitmqctl add_user gnuviechadmin ${RABBITMQ_DEFAULT_PASS}
rabbitmqctl list_vhosts --quiet | grep -q "gva" || \
  rabbitmqctl add_vhost gva
rabbitmqctl set_permissions --vhost gva gnuviechadmin '.*' '.*' '.*'
rabbitmqctl shutdown

sleep 2
su -s /bin/sh -c '/usr/sbin/rabbitmq-server' rabbitmq