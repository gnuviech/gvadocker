#!/bin/sh

set -e

su -s /bin/sh -c '/usr/bin/redis-server /etc/redis/redis.conf' redis
