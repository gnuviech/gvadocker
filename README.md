# gnuviechadmin docker

This repository contains a docker-compose setup for infrastructure required by
the gnuviechadmin tools.

## Build the docker images

To build the docker images required by the gnuviechadmin tools run:

```
docker-compose build
```
