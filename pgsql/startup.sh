#!/bin/sh

set -e

/usr/bin/pg_ctlcluster 11 main start

echo -n "Waiting for database to become ready ."
while ! pg_isready -q; do sleep 1; echo -n "."; done
echo " done"

if su - -c "psql -l" postgres | grep -q gnuviechadmin; then
   echo database gnuviechadmin exists
else
   su - -c "psql -e" postgres <<EOF
CREATE USER gnuviechadmin CREATEDB LOGIN PASSWORD '${GVA_PGSQL_PASSWORD}';
CREATE DATABASE gnuviechadmin TEMPLATE template0 ENCODING utf8 LC_COLLATE 'C.UTF-8' LC_CTYPE 'C.UTF-8';
GRANT CREATE, CONNECT ON DATABASE gnuviechadmin TO gnuviechadmin;
EOF
fi

/usr/bin/pg_ctlcluster 11 main stop --foreground
/usr/bin/pg_ctlcluster 11 main start --foreground -o '-h 0.0.0.0'
